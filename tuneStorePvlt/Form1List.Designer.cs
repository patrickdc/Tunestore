﻿namespace tuneStorePvlt
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.tabControlPvlt = new System.Windows.Forms.TabControl();
            this.tbpgOnePvlt = new System.Windows.Forms.TabPage();
            this.pbxOnePvlt = new System.Windows.Forms.PictureBox();
            this.pgbarOnePvlt = new System.Windows.Forms.ProgressBar();
            this.tbpgTwoPvlt = new System.Windows.Forms.TabPage();
            this.btnReversePvlt = new System.Windows.Forms.Button();
            this.btnFFPvlt = new System.Windows.Forms.Button();
            this.pgbSongPvlt = new System.Windows.Forms.ProgressBar();
            this.lblSearchPvlt = new System.Windows.Forms.Label();
            this.tbxSearchPvlt = new System.Windows.Forms.TextBox();
            this.btnNextSongPvlt = new System.Windows.Forms.Button();
            this.btnPreviousPvlt = new System.Windows.Forms.Button();
            this.btnShufflePvlt = new System.Windows.Forms.Button();
            this.lblTextPvlt = new System.Windows.Forms.Label();
            this.rtbTextboxPvlt = new System.Windows.Forms.ListBox();
            this.btnPlayNumberPvlt = new System.Windows.Forms.Button();
            this.tbxNumberPvlt = new System.Windows.Forms.TextBox();
            this.btnResumePvlt = new System.Windows.Forms.Button();
            this.btnPauseSongPvlt = new System.Windows.Forms.Button();
            this.btnStopSong = new System.Windows.Forms.Button();
            this.tmrOnePvlt = new System.Windows.Forms.Timer(this.components);
            this.menuStrip2 = new System.Windows.Forms.MenuStrip();
            this.fileTlstrPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAboutPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnHelpPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnLocatePvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.btnExitPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.songsTlstrPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.strpAddSongPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.strpPlaylistPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.strpLoadPlaylistPvlt = new System.Windows.Forms.ToolStripMenuItem();
            this.tmrAudioPvlt = new System.Windows.Forms.Timer(this.components);
            this.tabControlPvlt.SuspendLayout();
            this.tbpgOnePvlt.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pbxOnePvlt)).BeginInit();
            this.tbpgTwoPvlt.SuspendLayout();
            this.menuStrip2.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControlPvlt
            // 
            this.tabControlPvlt.Controls.Add(this.tbpgOnePvlt);
            this.tabControlPvlt.Controls.Add(this.tbpgTwoPvlt);
            this.tabControlPvlt.Location = new System.Drawing.Point(-3, 27);
            this.tabControlPvlt.Name = "tabControlPvlt";
            this.tabControlPvlt.SelectedIndex = 0;
            this.tabControlPvlt.Size = new System.Drawing.Size(593, 566);
            this.tabControlPvlt.TabIndex = 0;
            // 
            // tbpgOnePvlt
            // 
            this.tbpgOnePvlt.AccessibleName = "tabControlOnePvlt";
            this.tbpgOnePvlt.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.tbpgOnePvlt.Controls.Add(this.pbxOnePvlt);
            this.tbpgOnePvlt.Controls.Add(this.pgbarOnePvlt);
            this.tbpgOnePvlt.Location = new System.Drawing.Point(4, 22);
            this.tbpgOnePvlt.Name = "tbpgOnePvlt";
            this.tbpgOnePvlt.Padding = new System.Windows.Forms.Padding(3);
            this.tbpgOnePvlt.Size = new System.Drawing.Size(585, 540);
            this.tbpgOnePvlt.TabIndex = 0;
            this.tbpgOnePvlt.Text = "Loading";
            this.tbpgOnePvlt.UseVisualStyleBackColor = true;
            // 
            // pbxOnePvlt
            // 
            this.pbxOnePvlt.Image = global::tuneStorePvlt.Properties.Resources.logo;
            this.pbxOnePvlt.Location = new System.Drawing.Point(6, 7);
            this.pbxOnePvlt.Name = "pbxOnePvlt";
            this.pbxOnePvlt.Size = new System.Drawing.Size(571, 447);
            this.pbxOnePvlt.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pbxOnePvlt.TabIndex = 2;
            this.pbxOnePvlt.TabStop = false;
            // 
            // pgbarOnePvlt
            // 
            this.pgbarOnePvlt.Location = new System.Drawing.Point(6, 460);
            this.pgbarOnePvlt.Name = "pgbarOnePvlt";
            this.pgbarOnePvlt.Size = new System.Drawing.Size(571, 29);
            this.pgbarOnePvlt.TabIndex = 1;
            // 
            // tbpgTwoPvlt
            // 
            this.tbpgTwoPvlt.AccessibleName = "tabControlTwoPvlt";
            this.tbpgTwoPvlt.Controls.Add(this.btnReversePvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnFFPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.pgbSongPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.lblSearchPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.tbxSearchPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnNextSongPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnPreviousPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnShufflePvlt);
            this.tbpgTwoPvlt.Controls.Add(this.lblTextPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.rtbTextboxPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnPlayNumberPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.tbxNumberPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnResumePvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnPauseSongPvlt);
            this.tbpgTwoPvlt.Controls.Add(this.btnStopSong);
            this.tbpgTwoPvlt.Location = new System.Drawing.Point(4, 22);
            this.tbpgTwoPvlt.Name = "tbpgTwoPvlt";
            this.tbpgTwoPvlt.Padding = new System.Windows.Forms.Padding(3);
            this.tbpgTwoPvlt.Size = new System.Drawing.Size(585, 540);
            this.tbpgTwoPvlt.TabIndex = 1;
            this.tbpgTwoPvlt.Text = "Main";
            this.tbpgTwoPvlt.UseVisualStyleBackColor = true;
            // 
            // btnReversePvlt
            // 
            this.btnReversePvlt.Location = new System.Drawing.Point(102, 489);
            this.btnReversePvlt.Name = "btnReversePvlt";
            this.btnReversePvlt.Size = new System.Drawing.Size(69, 32);
            this.btnReversePvlt.TabIndex = 11;
            this.btnReversePvlt.Text = "Reverse";
            this.btnReversePvlt.UseVisualStyleBackColor = true;
            // 
            // btnFFPvlt
            // 
            this.btnFFPvlt.Location = new System.Drawing.Point(12, 489);
            this.btnFFPvlt.Name = "btnFFPvlt";
            this.btnFFPvlt.Size = new System.Drawing.Size(84, 32);
            this.btnFFPvlt.TabIndex = 10;
            this.btnFFPvlt.Text = "Fast Forward";
            this.btnFFPvlt.UseVisualStyleBackColor = true;
            // 
            // pgbSongPvlt
            // 
            this.pgbSongPvlt.Location = new System.Drawing.Point(11, 422);
            this.pgbSongPvlt.Name = "pgbSongPvlt";
            this.pgbSongPvlt.Size = new System.Drawing.Size(561, 23);
            this.pgbSongPvlt.TabIndex = 9;
            // 
            // lblSearchPvlt
            // 
            this.lblSearchPvlt.AutoSize = true;
            this.lblSearchPvlt.Location = new System.Drawing.Point(420, 461);
            this.lblSearchPvlt.Name = "lblSearchPvlt";
            this.lblSearchPvlt.Size = new System.Drawing.Size(44, 13);
            this.lblSearchPvlt.TabIndex = 8;
            this.lblSearchPvlt.Text = "Search:";
            // 
            // tbxSearchPvlt
            // 
            this.tbxSearchPvlt.Location = new System.Drawing.Point(470, 458);
            this.tbxSearchPvlt.Name = "tbxSearchPvlt";
            this.tbxSearchPvlt.Size = new System.Drawing.Size(103, 20);
            this.tbxSearchPvlt.TabIndex = 7;
            this.tbxSearchPvlt.TextChanged += new System.EventHandler(this.tbxSearchPvlt_TextChanged);
            // 
            // btnNextSongPvlt
            // 
            this.btnNextSongPvlt.Location = new System.Drawing.Point(303, 489);
            this.btnNextSongPvlt.Name = "btnNextSongPvlt";
            this.btnNextSongPvlt.Size = new System.Drawing.Size(86, 32);
            this.btnNextSongPvlt.TabIndex = 5;
            this.btnNextSongPvlt.Text = "Next Song";
            this.btnNextSongPvlt.UseVisualStyleBackColor = true;
            this.btnNextSongPvlt.Click += new System.EventHandler(this.btnNextSongPvlt_Click);
            // 
            // btnPreviousPvlt
            // 
            this.btnPreviousPvlt.Location = new System.Drawing.Point(487, 489);
            this.btnPreviousPvlt.Name = "btnPreviousPvlt";
            this.btnPreviousPvlt.Size = new System.Drawing.Size(86, 32);
            this.btnPreviousPvlt.TabIndex = 6;
            this.btnPreviousPvlt.Text = "Previous Song";
            this.btnPreviousPvlt.UseVisualStyleBackColor = true;
            this.btnPreviousPvlt.Click += new System.EventHandler(this.btnPreviousSongPvlt_Click);
            // 
            // btnShufflePvlt
            // 
            this.btnShufflePvlt.Location = new System.Drawing.Point(395, 489);
            this.btnShufflePvlt.Name = "btnShufflePvlt";
            this.btnShufflePvlt.Size = new System.Drawing.Size(86, 32);
            this.btnShufflePvlt.TabIndex = 6;
            this.btnShufflePvlt.Text = "Shuffle Song";
            this.btnShufflePvlt.UseVisualStyleBackColor = true;
            this.btnShufflePvlt.Click += new System.EventHandler(this.btnShufflePvlt_Click);
            // 
            // lblTextPvlt
            // 
            this.lblTextPvlt.Location = new System.Drawing.Point(9, 461);
            this.lblTextPvlt.Name = "lblTextPvlt";
            this.lblTextPvlt.Size = new System.Drawing.Size(253, 13);
            this.lblTextPvlt.TabIndex = 4;
            this.lblTextPvlt.Text = "...";
            // 
            // rtbTextboxPvlt
            // 
            this.rtbTextboxPvlt.Location = new System.Drawing.Point(11, 44);
            this.rtbTextboxPvlt.Name = "rtbTextboxPvlt";
            this.rtbTextboxPvlt.Size = new System.Drawing.Size(561, 368);
            this.rtbTextboxPvlt.TabIndex = 1;
            this.rtbTextboxPvlt.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.rtbTextboxPvlt_MouseDoubleClick);
            // 
            // btnPlayNumberPvlt
            // 
            this.btnPlayNumberPvlt.Location = new System.Drawing.Point(339, 451);
            this.btnPlayNumberPvlt.Name = "btnPlayNumberPvlt";
            this.btnPlayNumberPvlt.Size = new System.Drawing.Size(75, 32);
            this.btnPlayNumberPvlt.TabIndex = 3;
            this.btnPlayNumberPvlt.Text = "Play Number";
            this.btnPlayNumberPvlt.UseVisualStyleBackColor = true;
            this.btnPlayNumberPvlt.Click += new System.EventHandler(this.btnPlayNumberPvlt_Click);
            // 
            // tbxNumberPvlt
            // 
            this.tbxNumberPvlt.Location = new System.Drawing.Point(278, 458);
            this.tbxNumberPvlt.Name = "tbxNumberPvlt";
            this.tbxNumberPvlt.Size = new System.Drawing.Size(54, 20);
            this.tbxNumberPvlt.TabIndex = 2;
            // 
            // btnResumePvlt
            // 
            this.btnResumePvlt.Location = new System.Drawing.Point(161, 6);
            this.btnResumePvlt.Name = "btnResumePvlt";
            this.btnResumePvlt.Size = new System.Drawing.Size(69, 32);
            this.btnResumePvlt.TabIndex = 0;
            this.btnResumePvlt.Text = "Resume";
            this.btnResumePvlt.UseVisualStyleBackColor = true;
            this.btnResumePvlt.Click += new System.EventHandler(this.btnResumePvlt_Click);
            // 
            // btnPauseSongPvlt
            // 
            this.btnPauseSongPvlt.Location = new System.Drawing.Point(86, 6);
            this.btnPauseSongPvlt.Name = "btnPauseSongPvlt";
            this.btnPauseSongPvlt.Size = new System.Drawing.Size(69, 32);
            this.btnPauseSongPvlt.TabIndex = 0;
            this.btnPauseSongPvlt.Text = "Pause";
            this.btnPauseSongPvlt.UseVisualStyleBackColor = true;
            this.btnPauseSongPvlt.Click += new System.EventHandler(this.btnPauseSongPvlt_Click);
            // 
            // btnStopSong
            // 
            this.btnStopSong.Location = new System.Drawing.Point(11, 6);
            this.btnStopSong.Name = "btnStopSong";
            this.btnStopSong.Size = new System.Drawing.Size(69, 32);
            this.btnStopSong.TabIndex = 1;
            this.btnStopSong.Text = "Stop Song";
            this.btnStopSong.UseVisualStyleBackColor = true;
            this.btnStopSong.Click += new System.EventHandler(this.btnStopSong_Click);
            // 
            // tmrOnePvlt
            // 
            this.tmrOnePvlt.Interval = 10;
            this.tmrOnePvlt.Tick += new System.EventHandler(this.tmrOnePvlt_Tick);
            // 
            // menuStrip2
            // 
            this.menuStrip2.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.fileTlstrPvlt,
            this.songsTlstrPvlt});
            this.menuStrip2.Location = new System.Drawing.Point(0, 0);
            this.menuStrip2.Name = "menuStrip2";
            this.menuStrip2.Size = new System.Drawing.Size(586, 24);
            this.menuStrip2.TabIndex = 1;
            this.menuStrip2.Text = "menuStrip2";
            // 
            // fileTlstrPvlt
            // 
            this.fileTlstrPvlt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAboutPvlt,
            this.btnHelpPvlt,
            this.btnLocatePvlt,
            this.btnExitPvlt});
            this.fileTlstrPvlt.Name = "fileTlstrPvlt";
            this.fileTlstrPvlt.Size = new System.Drawing.Size(37, 20);
            this.fileTlstrPvlt.Text = "File";
            // 
            // btnAboutPvlt
            // 
            this.btnAboutPvlt.Name = "btnAboutPvlt";
            this.btnAboutPvlt.Size = new System.Drawing.Size(180, 22);
            this.btnAboutPvlt.Text = "About";
            this.btnAboutPvlt.Click += new System.EventHandler(this.btnAboutPvlt_Click);
            // 
            // btnHelpPvlt
            // 
            this.btnHelpPvlt.Name = "btnHelpPvlt";
            this.btnHelpPvlt.Size = new System.Drawing.Size(180, 22);
            this.btnHelpPvlt.Text = "Help";
            this.btnHelpPvlt.Click += new System.EventHandler(this.btnHelpPvlt_Click);
            // 
            // btnLocatePvlt
            // 
            this.btnLocatePvlt.Name = "btnLocatePvlt";
            this.btnLocatePvlt.Size = new System.Drawing.Size(180, 22);
            this.btnLocatePvlt.Text = "Locate";
            this.btnLocatePvlt.Click += new System.EventHandler(this.btnLocatePvlt_Click);
            // 
            // btnExitPvlt
            // 
            this.btnExitPvlt.Name = "btnExitPvlt";
            this.btnExitPvlt.Size = new System.Drawing.Size(180, 22);
            this.btnExitPvlt.Text = "Exit";
            this.btnExitPvlt.Click += new System.EventHandler(this.btnExitPvlt_Click);
            // 
            // songsTlstrPvlt
            // 
            this.songsTlstrPvlt.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.strpAddSongPvlt,
            this.strpPlaylistPvlt,
            this.strpLoadPlaylistPvlt});
            this.songsTlstrPvlt.Name = "songsTlstrPvlt";
            this.songsTlstrPvlt.Size = new System.Drawing.Size(51, 20);
            this.songsTlstrPvlt.Text = "Songs";
            // 
            // strpAddSongPvlt
            // 
            this.strpAddSongPvlt.Name = "strpAddSongPvlt";
            this.strpAddSongPvlt.Size = new System.Drawing.Size(180, 22);
            this.strpAddSongPvlt.Text = "Add Song";
            this.strpAddSongPvlt.Click += new System.EventHandler(this.btnAddSongPvlt_Click);
            // 
            // strpPlaylistPvlt
            // 
            this.strpPlaylistPvlt.Name = "strpPlaylistPvlt";
            this.strpPlaylistPvlt.Size = new System.Drawing.Size(180, 22);
            this.strpPlaylistPvlt.Text = "Save Playlist";
            this.strpPlaylistPvlt.Click += new System.EventHandler(this.btnSavePlaylistPvlt_Click);
            // 
            // strpLoadPlaylistPvlt
            // 
            this.strpLoadPlaylistPvlt.Name = "strpLoadPlaylistPvlt";
            this.strpLoadPlaylistPvlt.Size = new System.Drawing.Size(180, 22);
            this.strpLoadPlaylistPvlt.Text = "Load Playlist";
            this.strpLoadPlaylistPvlt.Click += new System.EventHandler(this.btnLoadPlaylistPvlt_Click);
            // 
            // tmrAudioPvlt
            // 
            this.tmrAudioPvlt.Tick += new System.EventHandler(this.tmrAudioPvlt_Tick);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(586, 608);
            this.Controls.Add(this.tabControlPvlt);
            this.Controls.Add(this.menuStrip2);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "Form1";
            this.Text = "Tunestore";
            this.tabControlPvlt.ResumeLayout(false);
            this.tbpgOnePvlt.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pbxOnePvlt)).EndInit();
            this.tbpgTwoPvlt.ResumeLayout(false);
            this.tbpgTwoPvlt.PerformLayout();
            this.menuStrip2.ResumeLayout(false);
            this.menuStrip2.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControlPvlt;
        private System.Windows.Forms.TabPage tbpgOnePvlt;
        private System.Windows.Forms.TabPage tbpgTwoPvlt;
        private System.Windows.Forms.ProgressBar pgbarOnePvlt;
        private System.Windows.Forms.Timer tmrOnePvlt;
        private System.Windows.Forms.PictureBox pbxOnePvlt;
        private System.Windows.Forms.Button btnPauseSongPvlt;
        private System.Windows.Forms.Button btnStopSong;
        private System.Windows.Forms.TextBox tbxNumberPvlt;
        private System.Windows.Forms.Button btnPlayNumberPvlt;
        private System.Windows.Forms.ListBox rtbTextboxPvlt;
        private System.Windows.Forms.Label lblTextPvlt;
        private System.Windows.Forms.MenuStrip menuStrip2;
        private System.Windows.Forms.ToolStripMenuItem fileTlstrPvlt;
        private System.Windows.Forms.ToolStripMenuItem songsTlstrPvlt;
        private System.Windows.Forms.ToolStripMenuItem btnHelpPvlt;
        private System.Windows.Forms.ToolStripMenuItem btnLocatePvlt;
        private System.Windows.Forms.ToolStripMenuItem btnExitPvlt;
        private System.Windows.Forms.ToolStripMenuItem strpAddSongPvlt;
        private System.Windows.Forms.ToolStripMenuItem strpPlaylistPvlt;
        private System.Windows.Forms.ToolStripMenuItem strpLoadPlaylistPvlt;
        private System.Windows.Forms.Button btnNextSongPvlt;
        private System.Windows.Forms.Button btnShufflePvlt;
        private System.Windows.Forms.Label lblSearchPvlt;
        private System.Windows.Forms.TextBox tbxSearchPvlt;
        private System.Windows.Forms.ToolStripMenuItem btnAboutPvlt;
        private System.Windows.Forms.Button btnResumePvlt;
        private System.Windows.Forms.Button btnPreviousPvlt;
        private System.Windows.Forms.Timer tmrAudioPvlt;
        private System.Windows.Forms.ProgressBar pgbSongPvlt;
        private System.Windows.Forms.Button btnReversePvlt;
        private System.Windows.Forms.Button btnFFPvlt;
    }
}

