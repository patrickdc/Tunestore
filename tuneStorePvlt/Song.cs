﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tuneStorePvlt
{
    class SongPvlt //song class
    {
        //song attributes
        private int songIdPvlt;
        private string songFileNamePvlt;
        private string songPathPvlt;

        //song constructor
        public SongPvlt(int c_songIdPvlt, string c_songFileNamePvlt, string c_songPathPvlt)
        {
            songIdPvlt = c_songIdPvlt;
            songFileNamePvlt = c_songFileNamePvlt;
            songPathPvlt = c_songPathPvlt;
        }

        //song path creator and retriever
        public string SongPathPvlt
        {
            get { return songPathPvlt; }
            set { songPathPvlt = value; }
        }

        //song file name creator and retriever
        public string SongFileNamePvlt
        {
            get { return songFileNamePvlt; }
            set { songFileNamePvlt = value; }
        }

        //song ID creator and retriever
        public int SongIdPvlt
        {
            get { return songIdPvlt; }
            set { songIdPvlt = value; }
        }
    }
}
