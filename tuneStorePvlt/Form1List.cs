﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;//audio
using System.IO;//streamreader

namespace tuneStorePvlt
{
    public partial class Form1 : Form
    {
        //Variables setup
        #region Audio Setup
        //Audio DLL import
        [DllImport("winmm.dll")]

        //default audio handling code
        private static extern long
        mciSendString(string strCommand,
        StringBuilder strReturn,
        int iReturnLength,
        IntPtr hwndCallback);
        #endregion

        List<SongPvlt> songListPvlt = new List<SongPvlt>();
        List<string> listPvlt = new List<string>();
        List<SongPvlt> searchListPvlt = new List<SongPvlt>();

        int songIdPvlt = 0;
        string songPathPvlt = "";
        string songNamePvlt = "";

        int selectedLinePvlt = 0;
        int saveIdPvlt = 0;
        string selectedNamePvlt = "";
        bool searchingPvlt = false;
        static bool stopMusicPvlt = false;
        int songLenthPvlt = 0;

        public Form1()
        {
            InitializeComponent();
            tmrOnePvlt.Start();
            tabControlPvlt.Appearance = TabAppearance.FlatButtons;
            tabControlPvlt.ItemSize = new Size(0, 1);
            tabControlPvlt.SizeMode = TabSizeMode.Fixed;
        }

        //Timer tick at loading screen, changes to main menu
        private void tmrOnePvlt_Tick(object sender, EventArgs e)
        {
            if (pgbarOnePvlt.Value < 100)
            {
                try
                {
                    pgbarOnePvlt.Value = pgbarOnePvlt.Value + 10;
                }
                catch
                {
                    MessageBox.Show("Progressbar incorrect value.");
                }
            }

            if (pgbarOnePvlt.Value == 100)
            {
                tabControlPvlt.SelectedIndex = 1;
            }
        }

        private void btnAddSongPvlt_Click(object sender, EventArgs e)
        {
            AddSongDialogPvlt();
        }

        private void AddSongDialogPvlt()
        {
            //opens file dialog with one file select
            OpenFileDialog fileDialogPvlt = new OpenFileDialog();
            fileDialogPvlt.Filter = "All Files (*.*)|*.*";
            fileDialogPvlt.FilterIndex = 1;
            fileDialogPvlt.Multiselect = false;
            //sends file information to variables and adds it to list
            if (fileDialogPvlt.ShowDialog() == DialogResult.OK)
            {
                songPathPvlt = fileDialogPvlt.FileName;
                songNamePvlt = System.IO.Path.GetFileNameWithoutExtension(songPathPvlt);
                AddSongToProgPvlt();
            }
        }

        private void rtbTextboxPvlt_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SelectSong();
            PlayMusic();
        }

        private void btnPlayNumberPvlt_Click(object sender, EventArgs e)
        {
            try
            {
                //checks if theres a valid song to select with the textbox input
                if (Convert.ToInt32(tbxNumberPvlt.Text) >= 0 && Convert.ToInt32(tbxNumberPvlt.Text) <= songListPvlt.Count() - 1)
                {
                    selectedLinePvlt = Convert.ToInt32(tbxNumberPvlt.Text);
                    PlayMusic();
                }
            }

            catch
            {
                MessageBox.Show("Please enter a valid song ID.");
            }
        }

        private void addSongToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddSongDialogPvlt();
        }


        private void tbxSearchPvlt_TextChanged(object sender, EventArgs e)
        {
            rtbTextboxPvlt.Items.Clear(); //Clears Box
            searchListPvlt.Clear(); //Clears search list

            string line = tbxSearchPvlt.Text.ToLower(); //Searchbox value

            foreach (string songListPvlt in listPvlt) //Loops through song array
            {
                if (songListPvlt.ToLower().IndexOf(line.ToLower()) > -1) // ???
                {
                    rtbTextboxPvlt.Items.Add(songListPvlt); //Adds new item to textbox
                    searchListPvlt.Add(new SongPvlt(songIdPvlt, songNamePvlt, songPathPvlt)); //Adds found items to search list
                }
            }

            //sets searching bool to true or false when searching, so the app handles accordingly
            if (!String.IsNullOrEmpty(tbxSearchPvlt.Text))
            {
                searchingPvlt = true;
            }

            else
            {
                searchingPvlt = false;
            }
        }

        private void btnAboutPvlt_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This application was made possible by \n \tPatrick Veltrop");
        }

        private void btnExitPvlt_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnHelpPvlt_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"..\..\manual\Manual.pdf");
        }

        private void btnLocatePvlt_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Application.ExecutablePath);
        }

        private void btnSavePlaylistPvlt_Click(object sender, EventArgs e)
        {
            try
            {
                StopMusic();
                SaveFileDialog saveFilePvlt = new SaveFileDialog();
                saveFilePvlt.Filter = "txt files (*.txt)|*.txt|All files (*.*)|*.*";
                //Display dialog and see if OK button was pressed
                if (saveFilePvlt.ShowDialog() == DialogResult.OK)
                {
                    //Save file to file name specified to SafeFileDialog
                    StreamWriter txtWritePvlt = new StreamWriter(saveFilePvlt.FileName);

                    //loops through every item in the textbox and saves them to a txt file
                    foreach (SongPvlt item in searchListPvlt)
                    {
                        if (item.SongFileNamePvlt == songListPvlt[item.SongIdPvlt].SongFileNamePvlt && item.SongPathPvlt == songListPvlt[item.SongIdPvlt].SongPathPvlt)
                        {
                            txtWritePvlt.WriteLine("#songID:" + searchListPvlt[saveIdPvlt].SongIdPvlt + "\n" + "#songName:" + searchListPvlt[saveIdPvlt].SongFileNamePvlt + "\n" + "#songPath:" + searchListPvlt[saveIdPvlt].SongPathPvlt);
                        }
                        saveIdPvlt++;
                    }

                    txtWritePvlt.Close();
                }
            }
            catch
            {

            }
        }

        private void btnLoadPlaylistPvlt_Click(object sender, EventArgs e)
        {
            //stops playing music and opens file dialog
            StopMusic();
            OpenFileDialog fileDialogPvlt = new OpenFileDialog();
            fileDialogPvlt.Filter = "All Files (*.*)|*.*";
            fileDialogPvlt.FilterIndex = 1;
            fileDialogPvlt.Multiselect = false;

            if (fileDialogPvlt.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    //variables for songs to load
                    int txtIDPvlt = 0;
                    int songIdPvlt = 0;
                    string txtNamePvlt = "";
                    string txtPathPvlt = "";
                    bool idPvlt = false;
                    bool namePvlt = false;
                    bool pathPvlt = false;

                    //clears textbox and lists to start fresh
                    rtbTextboxPvlt.Items.Clear();
                    listPvlt.Clear();
                    songListPvlt.Clear();
                    searchListPvlt.Clear();

                    //loops through every line in the playlist txt and assigns variables to the correct part in the lines
                    using (StreamReader reader = new StreamReader(fileDialogPvlt.FileName))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {

                            if (line.Contains("#songID:"))
                            {
                                songIdPvlt = txtIDPvlt;
                                txtIDPvlt++;
                                idPvlt = true;
                            }

                            if (line.Contains("#songName:"))
                            {
                                txtNamePvlt = line.Replace("#songName:","");
                                songNamePvlt = txtNamePvlt;
                                namePvlt = true;
                            }

                            if (line.Contains("#songPath:"))
                            {
                                txtPathPvlt = line.Replace("#songPath:", "");
                                songPathPvlt = txtPathPvlt;
                                pathPvlt = true;
                            }

                            if (idPvlt == true && namePvlt == true && pathPvlt == true)
                            {
                                AddSongToProgPvlt();
                                idPvlt = false;
                                namePvlt = false;
                                pathPvlt = false;
                            }
                        }
                    }
                }

                catch
                {
                    MessageBox.Show("An error occured while loading the song list.");
                }
            }
        }

        private void btnStopSong_Click(object sender, EventArgs e)
        {
            StopMusic();
        }

        private void btnPauseSongPvlt_Click(object sender, EventArgs e)
        {
            mciSendString("pause MediaFile", null, 0, IntPtr.Zero);
        }

        private void btnResumePvlt_Click(object sender, EventArgs e)
        {
            mciSendString("resume MediaFile", null, 0, IntPtr.Zero);
        }

        private void btnNextSongPvlt_Click(object sender, EventArgs e)
        {
            NextSong();
        }

        private void NextSong()
        {
            //if the selected line is lower than the max songs, automatically increments, or jumps to 0 if not
            if (selectedLinePvlt < songListPvlt.Count() - 1)
            {
                selectedLinePvlt++;
            }
            else
            {
                selectedLinePvlt = 0;
            }
            PlayMusic();
        }

        private void btnPreviousSongPvlt_Click(object sender, EventArgs e)
        {
            //checks if selected line is usable, if not it jumps to the newest song
            if (selectedLinePvlt > 0)
            {
                selectedLinePvlt--;
            }
            else
            {
                selectedLinePvlt = songListPvlt.Count() - 1;
            }
            PlayMusic();
        }

        private void btnShufflePvlt_Click(object sender, EventArgs e)
        {
            Random randomNumberGenPvlt = new Random();
            int randomNumberPvlt = randomNumberGenPvlt.Next(0, songListPvlt.Count());
            selectedLinePvlt = randomNumberPvlt;
            PlayMusic();
        }

        private void AddSongToProgPvlt()
        {
            rtbTextboxPvlt.Items.Add(songNamePvlt); // Add to textbox
            listPvlt.Add(songNamePvlt); // Add to list
            searchListPvlt.Add(new SongPvlt(songIdPvlt, songNamePvlt, songPathPvlt));
            songListPvlt.Add(new SongPvlt(songIdPvlt, songNamePvlt, songPathPvlt));
            songIdPvlt++;
        }

        private void SelectSong()
        {
            try
            {
                selectedLinePvlt = rtbTextboxPvlt.SelectedIndex;
                selectedNamePvlt = rtbTextboxPvlt.GetItemText(rtbTextboxPvlt.SelectedItem);
            }

            catch
            {

            }
        }

        private void PlayMusic()
        {
            try
            {
                StopMusic();

                while ((selectedNamePvlt != songListPvlt[selectedLinePvlt].SongFileNamePvlt) && searchingPvlt == true)
                {
                    selectedLinePvlt++;
                }

                if (songListPvlt.Count() > 0)
                {
                    //Play music from selection
                    mciSendString("open \" " + songListPvlt[selectedLinePvlt].SongPathPvlt + "\" " + "type mpegvideo alias MediaFile", null, 0, IntPtr.Zero);
                    mciSendString("play MediaFile", null, 0, IntPtr.Zero);
                    //show information of song
                    lblTextPvlt.Text = songListPvlt[selectedLinePvlt].SongFileNamePvlt;

                    CalculateLength();
                    if (searchingPvlt == false)
                    {
                        rtbTextboxPvlt.SetSelected(selectedLinePvlt, true);
                    }
                    tmrAudioPvlt.Start();
                }
            }

            catch
            {

            }
        }

        private void StopMusic()
        {
            //stops music and resets progressbar
            stopMusicPvlt = true;
            tmrAudioPvlt.Stop();
            pgbSongPvlt.Value = 0;
            mciSendString("close MediaFile", null, 0, IntPtr.Zero);
            stopMusicPvlt = false;
        }

        private void CalculateLength()
        {
            //calculates length to be used for the progressbar
            StringBuilder m_songLengthAsString = new StringBuilder(128);

            mciSendString("status MediaFile length", m_songLengthAsString, 128, IntPtr.Zero);

            songLenthPvlt = Convert.ToInt32(Convert.ToString(m_songLengthAsString));

            pgbSongPvlt.Maximum = Convert.ToInt32(songLenthPvlt);
        }

        private void GetSongPosition()
        {
            //updates progressbar with songposition variable during playback
            if (stopMusicPvlt == false)
            {
                int m_songPosition;

                StringBuilder m_songPositionAsString = new StringBuilder(128);

                mciSendString("status MediaFile position", m_songPositionAsString, 128, IntPtr.Zero);

                m_songPosition = Convert.ToInt32(Convert.ToString(m_songPositionAsString));

                pgbSongPvlt.Value = m_songPosition;

                if (pgbSongPvlt.Value == pgbSongPvlt.Maximum) //plays Next song at the end
                {
                    NextSong();
                }
            }
        }

        private void tmrAudioPvlt_Tick(object sender, EventArgs e)
        {
            GetSongPosition();
        }
    }
}
