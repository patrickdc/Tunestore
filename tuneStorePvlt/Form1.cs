﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;//audio
using System.IO;//streamreader

namespace tuneStorePvlt
{
    public partial class Form1 : Form
    {
        #region Audio Setup
        //Audio DLL import
        [DllImport("winmm.dll")]

        //default audio handling code
        private static extern long
        mciSendString(string strCommand,
        StringBuilder strReturn,
        int iReturnLength,
        IntPtr hwndCallback);
        #endregion

        //SongPvlt[] songArrayPvlt = new SongPvlt[999];

        List<SongPvlt> songListPvlt = new List<SongPvlt>();

        List<string> listPvlt = new List<string>();

        int songIdPvlt = 0;
        int songAmountPvlt = 0;
        string songPathPvlt = "";
        string songNamePvlt = "";

        int selectedLinePvlt = 0;
        string selectedNamePvlt = "";
        bool selectItemPvlt = false; //checks if item has been selected
        bool nextPrevPressedPvlt = false; //checks if next or previous song has been selected

        public Form1()
        {
            InitializeComponent();
            tmrOnePvlt.Start();
            tabControlPvlt.Appearance = TabAppearance.FlatButtons;
            tabControlPvlt.ItemSize = new Size(0, 1);
            tabControlPvlt.SizeMode = TabSizeMode.Fixed;
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            if (pgbarOnePvlt.Value < 100)
            {
                try
                {
                    pgbarOnePvlt.Value = pgbarOnePvlt.Value + 10;
                }
                catch
                {
                    MessageBox.Show("Progressbar incorrect value.");
                }
            }

            if (pgbarOnePvlt.Value == 100)
            {
                tabControlPvlt.SelectedIndex = 1;
            }
        }

        private void btnAddSongPvlt_Click(object sender, EventArgs e)
        {
            AddSongDialogPvlt();
        }

        private void AddSongDialogPvlt()
        {
            OpenFileDialog fileDialogPvlt = new OpenFileDialog();
            fileDialogPvlt.Filter = "All Files (*.*)|*.*";
            fileDialogPvlt.FilterIndex = 1;
            fileDialogPvlt.Multiselect = false;
            if (fileDialogPvlt.ShowDialog() == DialogResult.OK)
            {
                songPathPvlt = fileDialogPvlt.FileName;
                songNamePvlt = System.IO.Path.GetFileNameWithoutExtension(songPathPvlt);
                AddSongToProgPvlt();
                songIdPvlt++;
            }
        }

        private void rtbTextboxPvlt_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            SelectSong();
            PlayMusic();
        }

        private void btnPlayNumberPvlt_Click(object sender, EventArgs e)
        {
            selectedLinePvlt = Convert.ToInt32 (tbxNumberPvlt.Text);
            PlayMusic();
        }

        private void addSongToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddSongDialogPvlt();
        }


        private void tbxSearchPvlt_TextChanged(object sender, EventArgs e)
        {
            //loops through list every time even when empty, then compares searchbox value to items in list
            rtbTextboxPvlt.Items.Clear(); // Clears Box
            string line = tbxSearchPvlt.Text.ToLower(); // Searchbox value

            foreach (string songListPvlt in listPvlt) // Loops through song array
            {
                if (songListPvlt.ToLower().IndexOf(line.ToLower()) > -1)
                {
                    rtbTextboxPvlt.Items.Add(songListPvlt); // Adds new item to textbox
                }
            }
        }

        private void btnAboutPvlt_Click(object sender, EventArgs e)
        {
            MessageBox.Show("This application was made possible by \n \tPatrick Veltrop");
        }

        private void btnExitPvlt_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void btnHelpPvlt_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(@"..\..\manual\Manual.pdf");
        }

        private void btnLocatePvlt_Click(object sender, EventArgs e)
        {
            MessageBox.Show(Application.ExecutablePath);
        }

        private void btnSavePlaylistPvlt_Click(object sender, EventArgs e)
        {
            int saveIdPvlt = 0;
            const string sPath = "save.txt";
            System.IO.StreamWriter SaveFile = new System.IO.StreamWriter(sPath);

            foreach (var item in rtbTextboxPvlt.Items)
            {
                //SaveFile.WriteLine("#songID:" + songArrayPvlt[saveIdPvlt].SongIdPvlt + "\n" + "#songName:" + songArrayPvlt[saveIdPvlt].SongFileNamePvlt + "\n" + "#songPath:" + songArrayPvlt[saveIdPvlt].SongPathPvlt);
                SaveFile.WriteLine("#songID:" + songListPvlt[saveIdPvlt].SongIdPvlt + "\n" + "#songName:" + songListPvlt[saveIdPvlt].SongFileNamePvlt + "\n" + "#songPath:" + songListPvlt[saveIdPvlt].SongPathPvlt);

                saveIdPvlt++;
            }

            SaveFile.Close();
        }

        private void btnLoadPlaylistPvlt_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialogPvlt = new OpenFileDialog();
            fileDialogPvlt.Filter = "All Files (*.*)|*.*";
            fileDialogPvlt.FilterIndex = 1;
            fileDialogPvlt.Multiselect = false;

            if (fileDialogPvlt.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                try
                {
                    int txtIDPvlt = 0;
                    string txtNamePvlt = "";
                    string txtPathPvlt = "";
                    bool idPvlt = false;
                    bool namePvlt = false;
                    bool pathPvlt = false;

                    rtbTextboxPvlt.Items.Clear();

                    using (StreamReader reader = new StreamReader(fileDialogPvlt.FileName))
                    {
                        string line;
                        while ((line = reader.ReadLine()) != null)
                        {

                            if (line.Contains("#songID:"))
                            {
                                songIdPvlt = txtIDPvlt;
                                txtIDPvlt++;
                                idPvlt = true;
                            }

                            if (line.Contains("#songName:"))
                            {
                                txtNamePvlt = line.Replace("#songName:","");
                                songNamePvlt = txtNamePvlt;
                                namePvlt = true;
                            }

                            if (line.Contains("#songPath:"))
                            {
                                txtPathPvlt = line.Replace("#songPath:", "");
                                songPathPvlt = txtPathPvlt;
                                pathPvlt = true;
                            }

                            if (idPvlt == true && namePvlt == true && pathPvlt == true)
                            {
                                AddSongToProgPvlt();
                                idPvlt = false;
                                namePvlt = false;
                                pathPvlt = false;
                            }
                        }
                    }
                }

                catch
                {
                    MessageBox.Show("An error occured while loading the song list.");
                }
            }
        }

        private void btnStopSong_Click(object sender, EventArgs e)
        {
            StopMusic();
        }

        private void btnPauseSongPvlt_Click(object sender, EventArgs e)
        {
            mciSendString("pause MediaFile", null, 0, IntPtr.Zero);
        }

        private void btnResumePvlt_Click(object sender, EventArgs e)
        {
            mciSendString("resume MediaFile", null, 0, IntPtr.Zero);
        }

        private void btnNextSongPvlt_Click(object sender, EventArgs e)
        {
            nextPrevPressedPvlt = true;

            if (selectedLinePvlt == null)
            {
                selectedLinePvlt = 0;
            }

            if (selectedLinePvlt >= songAmountPvlt)
            {
                selectedLinePvlt = 0;
            }
            else if (selectedLinePvlt <= songAmountPvlt)
            {
                selectedLinePvlt = songAmountPvlt;
            }
            else
            {
                selectedLinePvlt++;
            }
            PlayMusic();
            nextPrevPressedPvlt = false;
        }

        private void btnPreviousSongPvlt_Click(object sender, EventArgs e)
        {
            selectedLinePvlt--;
            PlayMusic();
        }

        private void btnShufflePvlt_Click(object sender, EventArgs e)
        {
            Random randomNumberGenPvlt = new Random();
            int randomNumberPvlt = randomNumberGenPvlt.Next(0, songAmountPvlt);
            selectedLinePvlt = randomNumberPvlt;
            PlayMusic();
        }

        private void AddSongToProgPvlt()
        {
            rtbTextboxPvlt.Items.Add(songNamePvlt); // Add to textbox
            listPvlt.Add(songNamePvlt); // Add to list
            //songArrayPvlt[songIdPvlt] = new SongPvlt(songIdPvlt, songNamePvlt, songPathPvlt); // Add with class to array
            songListPvlt[0] = new SongPvlt(songIdPvlt, songNamePvlt, songPathPvlt); // Add with class to array
            songAmountPvlt++;
        }

        private void SelectSong()
        {
            selectedLinePvlt = rtbTextboxPvlt.SelectedIndex;
            selectedNamePvlt = rtbTextboxPvlt.GetItemText(rtbTextboxPvlt.SelectedItem);
            selectItemPvlt = true;
        }

        private void PlayMusic()
        {
            StopMusic();
            //gets songID from selected line for the song array
            if (selectItemPvlt == true && nextPrevPressedPvlt == false)
            {
                while (selectedNamePvlt != songListPvlt[selectedLinePvlt].SongFileNamePvlt)
                {
                    selectedLinePvlt++;
                }

                //Play music from selection
                mciSendString("open \" " + songListPvlt[selectedLinePvlt].SongPathPvlt + "\" " + "type mpegvideo alias MediaFile", null, 0, IntPtr.Zero);
                mciSendString("play MediaFile", null, 0, IntPtr.Zero);
            }
            //show information of song
            if (songListPvlt.Count() > 0 && selectedLinePvlt < songListPvlt.Count())
            {
                lblTextPvlt.Text = songListPvlt[selectedLinePvlt].SongFileNamePvlt;
            }
        }

        private static void StopMusic()
        {
            mciSendString("close MediaFile", null, 0, IntPtr.Zero);
        }
    }
}
